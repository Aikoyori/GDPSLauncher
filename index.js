const {app,BrowserWindow,ipcMain,remote} = require('electron')
const log = require('electron-log');
var child_process = require('child_process');
const { autoUpdater } = require("electron-updater")

const Store = require('./store.js');
const store = new Store({
  // We'll call our data file 'user-preferences'
  configName: 'user-preferences',
  defaults: {
    // 800x600 is the default size of our window
    windowBounds: { width: 800, height: 600 },
  }
});

autoUpdater.logger = log;
autoUpdater.logger.transports.file.level = 'info';
log.info('App starting...'); 
let updateAvailable=false;
let mainWindow
function createWindow()
{
    let { width, height } = store.get('windowBounds');
    mainWindow=new BrowserWindow({
        width:width,
        height:height,
        minHeight:480,
        minWidth:640,
        icon: __dirname + '/favicon.ico',
        frame:false,
        nodeIntegration: true,
        webPreferences: {
            nodeIntegration: true
          }
    })
    mainWindow.loadFile('index.html')
    
    mainWindow.on('closed', function () {
      mainWindow = null
        
    })
    mainWindow.on('resize', () => {
      let { width, height } = mainWindow.getBounds();
      store.set('windowBounds', { width, height });
    });
    mainWindow.setMenu(null)
//mainWindow.webContents.openDevTools()
function sendStatusToWindow(text) {
    log.info(text);
    mainWindow.webContents.send('message', text);
}
autoUpdater.autoDownload = false


autoUpdater.on('checking-for-update', () => {
  sendStatusToWindow('Checking for update...');
})
autoUpdater.on('update-available', (info) => {
  sendStatusToWindow('Update available. Click here to install.');
  updateAvailable=true;
})
autoUpdater.on('update-not-available', (info) => {
  sendStatusToWindow('');
})
autoUpdater.on('error', (err) => {
  sendStatusToWindow('Error in auto-updater.');
  console.log(err)
})
autoUpdater.on('download-progress', (progressObj) => {
    let log_message = " " + progressObj.bytesPerSecond+"B/s";
    log_message = log_message + ' - ' + progressObj.percent + '%';
    sendStatusToWindow(log_message);
})
autoUpdater.on('update-downloaded', (info) => {
  sendStatusToWindow('Update downloaded. Close to install');
  
});

}
ipcMain.on('asynchronous-message', function (evt, message) {
    if (message == 'downloadUpdate'&&updateAvailable) {
        updateAvailable=false;  
        autoUpdater.downloadUpdate()

    }
});
app.on('ready', createWindow)
app.on('ready', function()  {
    autoUpdater.checkForUpdatesAndNotify();
  });
app.on('window-all-closed', function () {
    if (process.platform !== 'darwin') app.quit()
    mainWindow.webContents.session.clearCache(function(){});
  })
app.on('activate', function () {
if (mainWindow === null) createWindow()
})
