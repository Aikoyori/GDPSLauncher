const {remote } = require('electron')
const electron = require('electron')
const customTitlebar = require('custom-electron-titlebar')
var child_process = require('promisify-child-process');
var $ = require('jQuery')
var pjson = require('./package.json');
const Store = require('./store.js');
const store = new Store({
    configName: 'theme',
    defaults: {
      dark:false,
    }
});
var darkyes= store.get('dark')
    if (darkyes)
    {
        updateColor(darkyes)
        store.set('dark',true);
    }
      else
      {
        updateColor(darkyes)
        store.set('dark',false);

      }
 
 
var titlebar = new customTitlebar.Titlebar({
    backgroundColor: customTitlebar.Color.fromHex('#f8f9fa'),
    icon: 'qrtzgdps.png',
    menuPosition:'bottom',
    titleHorizontalAlignment:'left',
});




titlebar.updateTitle("QrtZGDPS Launcher "+pjson.version)
var x=new remote.Menu()
x.append(new remote.MenuItem({
    label: 'Menu',
    submenu: [
        {
            label: 'Home',
            click: () => document.getElementById('cont').src='https://news.qwty.cc'
        },      
        {
            label: 'GDPS Dashboard',
            click: () => document.getElementById('cont').src='https://qrtzgdps.qwty.cc/database/dashboard'
        },            
        {
            label: 'GDPS Tools',
            click: () => document.getElementById('cont').src='https://qrtzgdps.qwty.cc/database/tools'
        },         
        {
            label: 'QrtZ\'s Discord',
            click: () => remote.shell.openExternal('https://dc.qwty.cc/')
        },
        {
            type:'separator'
        },
        {
            label: 'Reload',
            role:'reload'
        },
        {
            label: 'Toggle DevTools',
            role:'toggledevtools'
        },
    ]
}))

titlebar.updateMenu(x);

function initialize() {

    return new Promise(function(resolve, reject) {
    	// Do async job
        fun()
    })
}
$(".titlebar").off()
let heck
let mainWindow
async function fun(){
    console.log("fun() start");
    var commandtoRun = 'QrtZGDPSDash.exe'; 

    var child=child_process.spawn(commandtoRun,{
         cwd:'game',
         detached: true,
         stdio: [null, null, null, 'ipc']
       }).unref()

 }
mainWindow=remote.getCurrentWindow()
document.querySelector('#run').addEventListener('click', () => {
    fun();
})
document.querySelector('#discord').addEventListener('click', () => {
    remote.shell.openExternal('https://dc.qwty.cc/')
})
document.querySelector('#main').addEventListener('click', () => {
    document.getElementById('cont').src='https://news.qwty.cc'
})
document.querySelector('#update').addEventListener('click', () => {
    ipcRenderer.send('asynchronous-message', 'downloadUpdate');
})


