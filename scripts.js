
var $ = require('jQuery')
const Store = require('./store.js');

      const store2 = new Store({
    configName: 'theme',
    defaults: {
      dark:false,
    }
}); 
var darkOk=store2.get('dark')
function updateColor(first=false)
{   
    console.log(darkOk)
    var receiver = document.getElementById('cont').contentWindow;
    if (darkOk)
    {
        $("#themebtn").text('Dark');
        darkOk=false;
        
        store2.set('dark',false);
        $("#titlebar").removeClass('bg-dark');
        $("#titlebar").addClass('bg-light');
        $(".titlebar").removeClass('bg-dark');
        $(".titlebar").addClass('bg-light');
        $(".titlebar").addClass('text-dark');
        $(".titlebar").removeClass('text-light');

        $(".titlebar").addClass('light');
        $(".titlebar").removeClass('dark');

        $("#whole").removeClass('bg-dark');
        $("#whole").addClass('bg-light');
        $(".text").addClass('text-dark');
        $(".text").removeClass('text-light');
    }
    else
    {
        $("#themebtn").text('Light');
        darkOk=true;
        store2.set('dark',true);
        $("#titlebar").addClass('bg-dark');
        $("#titlebar").removeClass('bg-light');
        $(".titlebar").addClass('bg-dark');
        $(".titlebar").removeClass('bg-light');
        $(".titlebar").removeClass('text-dark');
        $(".titlebar").addClass('text-light');
        $(".titlebar").addClass('dark');
        $(".titlebar").removeClass('light');
        
        $("#whole").addClass('bg-dark');
        $("#whole").removeClass('bg-light');
        $(".text").removeClass('text-dark');
        $(".text").addClass('text-light');
    }
    receiver.postMessage(darkOk, '*');
    
}
function load()
{    
    var receiver = document.getElementById('cont').contentWindow;
    receiver.postMessage(darkOk, '*');
    console.log("message send ")
}

window.onload=load